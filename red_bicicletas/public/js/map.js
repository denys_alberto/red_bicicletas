var map = L.map('main_map').setView([-16.5,-68.15], 13);

L.tileLayer('Https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',{
	attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributos'
}).addTo(map);

L.marker([-16.5151677,-68.1298805]).addTo(map)
L.marker([-16.5152192,-68.130076]).addTo(map)
L.marker([-16.5152869,-68.130076]).addTo(map)